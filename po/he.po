# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Bar Bokovza <bokovzabox@gmail.com>, 2015
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-11 22:31+0000\n"
"PO-Revision-Date: 2015-12-25 20:23+0000\n"
"Last-Translator: Bar Bokovza <bokovzabox@gmail.com>\n"
"Language-Team: Hebrew (http://www.transifex.com/mate/MATE/language/he/)\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../lib/mate-menu.py:70
msgid "Menu"
msgstr "תפריט"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:254
msgid "Couldn't load plugin:"
msgstr "לא ניתן להעלות את התוסף:"

#: ../lib/mate-menu.py:326
msgid "Couldn't initialize plugin"
msgstr "לא ניתן להפעיל את התוסף:"

#: ../lib/mate-menu.py:715
msgid "Advanced MATE Menu"
msgstr "תפריט MATE מתקדם"

#: ../lib/mate-menu.py:798
msgid "Preferences"
msgstr "העדפות"

#: ../lib/mate-menu.py:801
msgid "Edit menu"
msgstr "עריכת תפריט"

#: ../lib/mate-menu.py:804
msgid "Reload plugins"
msgstr "העלאת תוספים"

#: ../lib/mate-menu.py:807
msgid "About"
msgstr "אודות"

#. i18n
#: ../lib/mate-menu-config.py:50
msgid "Menu preferences"
msgstr "העדפות תפריט"

#: ../lib/mate-menu-config.py:53
msgid "Always start with favorites pane"
msgstr "תמיד הצג בתחילה את פאנל המועדפים"

#: ../lib/mate-menu-config.py:54
msgid "Show button icon"
msgstr "הצג סמליל כפתור"

#: ../lib/mate-menu-config.py:55
msgid "Use custom colors"
msgstr "השתמש בצבעים מותאמים אישית"

#: ../lib/mate-menu-config.py:56
msgid "Show recent documents plugin"
msgstr "הצג את תוסף הקבצים שהוצגו לאחרונה"

#: ../lib/mate-menu-config.py:57
msgid "Show applications plugin"
msgstr "הצג את תוסף האפליקציות"

#: ../lib/mate-menu-config.py:58
msgid "Show system plugin"
msgstr "הצג את תוסף המערכת"

#: ../lib/mate-menu-config.py:59
msgid "Show places plugin"
msgstr "הצג את תוסף המקומות"

#: ../lib/mate-menu-config.py:61
msgid "Show application comments"
msgstr "הצג את הערות היישומון"

#: ../lib/mate-menu-config.py:62
msgid "Show category icons"
msgstr "הצג סמלילי קטגוריות"

#: ../lib/mate-menu-config.py:63
msgid "Hover"
msgstr "עבור מעל"

#: ../lib/mate-menu-config.py:64
msgid "Remember the last category or search"
msgstr "זכור את הקטגוריה האחרונה או את החיפוש האחרון"

#: ../lib/mate-menu-config.py:65
msgid "Swap name and generic name"
msgstr "החלף בין שם לשם הגנרי"

#: ../lib/mate-menu-config.py:67
msgid "Border width:"
msgstr "רוחב מסגרת:"

#: ../lib/mate-menu-config.py:68
msgid "pixels"
msgstr "פיקסלים"

#: ../lib/mate-menu-config.py:70
msgid "Opacity:"
msgstr "אטימות:"

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr "טקסט כפתור:"

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "אפשרויות"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:252
msgid "Applications"
msgstr "אפליקציות"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr "עיצובים"

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:249
#: ../mate_menu/plugins/applications.py:250
msgid "Favorites"
msgstr "מועדפים"

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr "כפתור ראשי"

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr "תוספים"

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "רקע:"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr "כותרות:"

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr "מסגרות:"

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr "עיצוב:"

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr "מספר עמודות:"

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "גודל סמליל:"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr "זמן מעל (במיל' שניות):"

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "סמליל כפתור:"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr "פקודת חיפוש:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:58
msgid "Places"
msgstr "מקומות"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr "אפשר בר גלילה"

#: ../lib/mate-menu-config.py:100
msgid "Show GTK Bookmarks"
msgstr "הצג סימניות GTK"

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "גובה:"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr "מתג מקומות ברירת מחדל:"

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:153
msgid "Computer"
msgstr "מחשב"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:164
msgid "Home Folder"
msgstr "תיקיית הבית"

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:177
msgid "Network"
msgstr "רשת"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:198
msgid "Desktop"
msgstr "שולחן העבודה"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:209
msgid "Trash"
msgstr "פח האשפה"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr "מקומות מותאמים אישית:"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "מערכת"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr "מתג פריטים ברירת מחדל:"

#: ../lib/mate-menu-config.py:114
#: ../mate_menu/plugins/system_management.py:149
#: ../mate_menu/plugins/system_management.py:152
#: ../mate_menu/plugins/system_management.py:155
msgid "Package Manager"
msgstr "מנהל החבילות"

#: ../lib/mate-menu-config.py:115
#: ../mate_menu/plugins/system_management.py:162
msgid "Control Center"
msgstr "מרכז הבקרה"

#: ../lib/mate-menu-config.py:116
#: ../mate_menu/plugins/system_management.py:169
msgid "Terminal"
msgstr "מסוף"

#: ../lib/mate-menu-config.py:117
#: ../mate_menu/plugins/system_management.py:179
msgid "Lock Screen"
msgstr "מסך נעילה"

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "התנתק"

#: ../lib/mate-menu-config.py:119
#: ../mate_menu/plugins/system_management.py:197
msgid "Quit"
msgstr "צא"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr "ערוך מיקום"

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr "מיקום חדש"

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr "בחר תיקייה"

#: ../lib/mate-menu-config.py:152
msgid "Keyboard shortcut:"
msgstr "קיצור מקלדת:"

#: ../lib/mate-menu-config.py:158
msgid "Images"
msgstr "תמונות"

#: ../lib/mate-menu-config.py:267
msgid "Name"
msgstr "שם"

#: ../lib/mate-menu-config.py:268
msgid "Path"
msgstr "מסלול"

#: ../lib/mate-menu-config.py:284
msgid "Desktop theme"
msgstr "עיצוב שולחן העבודה"

#: ../lib/mate-menu-config.py:432 ../lib/mate-menu-config.py:463
msgid "Name:"
msgstr "שם:"

#: ../lib/mate-menu-config.py:433 ../lib/mate-menu-config.py:464
msgid "Path:"
msgstr "מסלול:"

#. i18n
#: ../mate_menu/plugins/applications.py:247
msgid "Search:"
msgstr "חיפוש:"

#: ../mate_menu/plugins/applications.py:251
msgid "All applications"
msgstr "כל האפליקציות"

#: ../mate_menu/plugins/applications.py:618
#, python-format
msgid "Search Google for %s"
msgstr "חפש באמצעות Google עבור %s"

#: ../mate_menu/plugins/applications.py:625
#, python-format
msgid "Search Wikipedia for %s"
msgstr "חפש באמצעות Wikipedia עבור %s"

#: ../mate_menu/plugins/applications.py:641
#, python-format
msgid "Lookup %s in Dictionary"
msgstr "חפש את %s במילון"

#: ../mate_menu/plugins/applications.py:648
#, python-format
msgid "Search Computer for %s"
msgstr "חפש במחשב עבור %s"

#. i18n
#: ../mate_menu/plugins/applications.py:761
#: ../mate_menu/plugins/applications.py:830
msgid "Add to desktop"
msgstr "הוסף לשולחן העבודה"

#: ../mate_menu/plugins/applications.py:762
#: ../mate_menu/plugins/applications.py:831
msgid "Add to panel"
msgstr "הוסף לפאנל"

#: ../mate_menu/plugins/applications.py:764
#: ../mate_menu/plugins/applications.py:812
msgid "Insert space"
msgstr "הכנס מרווח"

#: ../mate_menu/plugins/applications.py:765
#: ../mate_menu/plugins/applications.py:813
msgid "Insert separator"
msgstr "הכנס מפריד"

#: ../mate_menu/plugins/applications.py:767
#: ../mate_menu/plugins/applications.py:834
msgid "Launch when I log in"
msgstr "הפעל כאשר אני נכנס"

#: ../mate_menu/plugins/applications.py:769
#: ../mate_menu/plugins/applications.py:836
msgid "Launch"
msgstr "הפעל"

#: ../mate_menu/plugins/applications.py:770
msgid "Remove from favorites"
msgstr "מחק מהמועדפים"

#: ../mate_menu/plugins/applications.py:772
#: ../mate_menu/plugins/applications.py:839
msgid "Edit properties"
msgstr "ערוך הגדרות"

#. i18n
#: ../mate_menu/plugins/applications.py:811
msgid "Remove"
msgstr "מחק"

#: ../mate_menu/plugins/applications.py:833
msgid "Show in my favorites"
msgstr "הצג במועדפים"

#: ../mate_menu/plugins/applications.py:837
msgid "Delete from menu"
msgstr "מחק מהתפריט"

#: ../mate_menu/plugins/applications.py:888
msgid "Search Google"
msgstr "חפש ב-Google"

#: ../mate_menu/plugins/applications.py:895
msgid "Search Wikipedia"
msgstr "חפש ב-Wikipedia"

#: ../mate_menu/plugins/applications.py:905
msgid "Lookup Dictionary"
msgstr "חפש במילון"

#: ../mate_menu/plugins/applications.py:912
msgid "Search Computer"
msgstr "חפש במחשב"

#: ../mate_menu/plugins/applications.py:1332
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""
"לא ניתן לשמור מועדפים. בדוק אם הנך מחזיק בגישת כתיבה ל- ~/.config/mate-menu"

#: ../mate_menu/plugins/applications.py:1532
msgid "All"
msgstr "הכל"

#: ../mate_menu/plugins/applications.py:1532
msgid "Show all applications"
msgstr "הצג את כל האפליקציות"

#: ../mate_menu/plugins/system_management.py:159
msgid "Install, remove and upgrade software packages"
msgstr "התקן, מחר או עדכן חבילות תוכנה"

#: ../mate_menu/plugins/system_management.py:166
msgid "Configure your system"
msgstr "הגדר את המערכת"

#: ../mate_menu/plugins/system_management.py:176
msgid "Use the command line"
msgstr "השתמש בשורת הפקודה"

#: ../mate_menu/plugins/system_management.py:187
msgid "Requires password to unlock"
msgstr "נדרשת סיסמה לשחרור"

#: ../mate_menu/plugins/system_management.py:190
msgid "Logout"
msgstr "התנתק"

#: ../mate_menu/plugins/system_management.py:194
msgid "Log out or switch user"
msgstr "התנתק או החלף משתמש"

#: ../mate_menu/plugins/system_management.py:201
msgid "Shutdown, restart, suspend or hibernate"
msgstr "כיבוי, הדלקה מחדש, המתנה ומצב שינה"

#: ../mate_menu/plugins/places.py:161
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr ""
"עיין בכל הדיסקים המקומיים והמרוחקים, ובתיקיות הניתנות לגישה אליהן ממחשב זה"

#: ../mate_menu/plugins/places.py:172
msgid "Open your personal folder"
msgstr "פתח את התיקייה האישית שלך"

#: ../mate_menu/plugins/places.py:185
msgid "Browse bookmarked and local network locations"
msgstr "עיין במקומות רשת מקומיים ומרוחקים שנשמרו"

#: ../mate_menu/plugins/places.py:206
msgid "Browse items placed on the desktop"
msgstr "עיין בפריטים הנמצאים בשולחן העבודה"

#: ../mate_menu/plugins/places.py:219
msgid "Browse deleted files"
msgstr "עיין בקבצים מחוקים"

#: ../mate_menu/plugins/places.py:272
msgid "Empty trash"
msgstr "פח האשפה ריק"

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr "מסמכים שנראו לאחרונה"

#: ../mate_menu/keybinding.py:199
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr "לחץ להגדיר מאיץ חדש לפתיחת וסגירת התפריט."

#: ../mate_menu/keybinding.py:200
msgid "Press Escape or click again to cancel the operation.  "
msgstr "לחץ על Esc או לחץ שוב על \"ביטול\" כדי לבטל את הפעולה"

#: ../mate_menu/keybinding.py:201
msgid "Press Backspace to clear the existing keybinding."
msgstr "לחץ על Backspace על מנת לנקות קיצורי דרך קיימים."

#: ../mate_menu/keybinding.py:214
msgid "Pick an accelerator"
msgstr "בחר מאיץ"

#: ../mate_menu/keybinding.py:267
msgid "<not set>"
msgstr "<לא מוגדר>"
