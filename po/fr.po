# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Tubuntu <tubuntu@testimonium.be>, 2015
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-11 22:31+0000\n"
"PO-Revision-Date: 2016-01-28 21:20+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: French (http://www.transifex.com/mate/MATE/language/fr/)\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../lib/mate-menu.py:70
msgid "Menu"
msgstr "Menu"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:254
msgid "Couldn't load plugin:"
msgstr "Impossible de charger le greffon :"

#: ../lib/mate-menu.py:326
msgid "Couldn't initialize plugin"
msgstr "Impossible d'initialiser le greffon"

#: ../lib/mate-menu.py:715
msgid "Advanced MATE Menu"
msgstr "Menu MATE avancé"

#: ../lib/mate-menu.py:798
msgid "Preferences"
msgstr "Preferences"

#: ../lib/mate-menu.py:801
msgid "Edit menu"
msgstr "Modifier le menu"

#: ../lib/mate-menu.py:804
msgid "Reload plugins"
msgstr "Recharger les greffons"

#: ../lib/mate-menu.py:807
msgid "About"
msgstr "À propos"

#. i18n
#: ../lib/mate-menu-config.py:50
msgid "Menu preferences"
msgstr "Préférences du menu"

#: ../lib/mate-menu-config.py:53
msgid "Always start with favorites pane"
msgstr "Toujours démarrer avec le Tableau de bord des favoris"

#: ../lib/mate-menu-config.py:54
msgid "Show button icon"
msgstr "Afficher l'icône de bouton"

#: ../lib/mate-menu-config.py:55
msgid "Use custom colors"
msgstr "Utiliser des couleurs personnalisées"

#: ../lib/mate-menu-config.py:56
msgid "Show recent documents plugin"
msgstr "Afficher le greffon des documents récents"

#: ../lib/mate-menu-config.py:57
msgid "Show applications plugin"
msgstr "Afficher le greffon des applications"

#: ../lib/mate-menu-config.py:58
msgid "Show system plugin"
msgstr "Afficher le greffon du système"

#: ../lib/mate-menu-config.py:59
msgid "Show places plugin"
msgstr "Afficher le greffon des emplacements"

#: ../lib/mate-menu-config.py:61
msgid "Show application comments"
msgstr "Afficher les commentaires d'application"

#: ../lib/mate-menu-config.py:62
msgid "Show category icons"
msgstr "Afficher les icônes de catégorie"

#: ../lib/mate-menu-config.py:63
msgid "Hover"
msgstr "Survol de curseur"

#: ../lib/mate-menu-config.py:64
msgid "Remember the last category or search"
msgstr "Se souvenir de la dernière catégorie ou recherche"

#: ../lib/mate-menu-config.py:65
msgid "Swap name and generic name"
msgstr "Nom de rechange et nom générique"

#: ../lib/mate-menu-config.py:67
msgid "Border width:"
msgstr "Largeur de bordures :"

#: ../lib/mate-menu-config.py:68
msgid "pixels"
msgstr "pixels"

#: ../lib/mate-menu-config.py:70
msgid "Opacity:"
msgstr "Opacité :"

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr "Texte du bouton :"

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "Options"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:252
msgid "Applications"
msgstr "Applications"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr "Thème"

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:249
#: ../mate_menu/plugins/applications.py:250
msgid "Favorites"
msgstr "Favoris"

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr "Bouton principal"

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr "Greffons"

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "Arrière-plan :"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr "Titres :"

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr "Bordures :"

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr "Thème :"

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr "Nombre de colonnes :"

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "Taille de l'icône :"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr "Délai au survol de curseur (ms) :"

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "Icône de bouton :"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr "Commande de recherche :"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:58
msgid "Places"
msgstr "Emplacements"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr "Permettre la barre de défilement"

#: ../lib/mate-menu-config.py:100
msgid "Show GTK Bookmarks"
msgstr "Afficher les signets GTK"

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "Hauteur :"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr "Basculer les emplacements par défaut :"

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:153
msgid "Computer"
msgstr "Ordinateur"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:164
msgid "Home Folder"
msgstr "Dossier personnel"

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:177
msgid "Network"
msgstr "Réseau"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:198
msgid "Desktop"
msgstr "Bureau"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:209
msgid "Trash"
msgstr "Corbeille"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr "Emplacements personnalisés :"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "Système"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr "Basculer les éléments par défaut :"

#: ../lib/mate-menu-config.py:114
#: ../mate_menu/plugins/system_management.py:149
#: ../mate_menu/plugins/system_management.py:152
#: ../mate_menu/plugins/system_management.py:155
msgid "Package Manager"
msgstr "Gestionnaire des paquets"

#: ../lib/mate-menu-config.py:115
#: ../mate_menu/plugins/system_management.py:162
msgid "Control Center"
msgstr "Centre de contrôle"

#: ../lib/mate-menu-config.py:116
#: ../mate_menu/plugins/system_management.py:169
msgid "Terminal"
msgstr "Terminal"

#: ../lib/mate-menu-config.py:117
#: ../mate_menu/plugins/system_management.py:179
msgid "Lock Screen"
msgstr "Verrouiller l'écran"

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "Déconnexion"

#: ../lib/mate-menu-config.py:119
#: ../mate_menu/plugins/system_management.py:197
msgid "Quit"
msgstr "Quitter"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr "Modifier l'emplacement"

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr "Nouvel emplacement"

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr "Sélectionner un répertoire"

#: ../lib/mate-menu-config.py:152
msgid "Keyboard shortcut:"
msgstr "Raccourci clavier :"

#: ../lib/mate-menu-config.py:158
msgid "Images"
msgstr "Images"

#: ../lib/mate-menu-config.py:267
msgid "Name"
msgstr "Nom"

#: ../lib/mate-menu-config.py:268
msgid "Path"
msgstr "Chemin"

#: ../lib/mate-menu-config.py:284
msgid "Desktop theme"
msgstr "Thème du Bureau"

#: ../lib/mate-menu-config.py:432 ../lib/mate-menu-config.py:463
msgid "Name:"
msgstr "Nom :"

#: ../lib/mate-menu-config.py:433 ../lib/mate-menu-config.py:464
msgid "Path:"
msgstr "Chemin :"

#. i18n
#: ../mate_menu/plugins/applications.py:247
msgid "Search:"
msgstr "Recherche :"

#: ../mate_menu/plugins/applications.py:251
msgid "All applications"
msgstr "Toutes les applications"

#: ../mate_menu/plugins/applications.py:618
#, python-format
msgid "Search Google for %s"
msgstr "Rechercher %s sur Google"

#: ../mate_menu/plugins/applications.py:625
#, python-format
msgid "Search Wikipedia for %s"
msgstr "Rechercher %s sur Wikipédia"

#: ../mate_menu/plugins/applications.py:641
#, python-format
msgid "Lookup %s in Dictionary"
msgstr "Consulter %s dans Dictionnaire"

#: ../mate_menu/plugins/applications.py:648
#, python-format
msgid "Search Computer for %s"
msgstr "Rechercher %s sur l'ordinateur"

#. i18n
#: ../mate_menu/plugins/applications.py:761
#: ../mate_menu/plugins/applications.py:830
msgid "Add to desktop"
msgstr "Ajouter au Bureau"

#: ../mate_menu/plugins/applications.py:762
#: ../mate_menu/plugins/applications.py:831
msgid "Add to panel"
msgstr "Ajouter au Tableau de bord"

#: ../mate_menu/plugins/applications.py:764
#: ../mate_menu/plugins/applications.py:812
msgid "Insert space"
msgstr "Insérer un espace"

#: ../mate_menu/plugins/applications.py:765
#: ../mate_menu/plugins/applications.py:813
msgid "Insert separator"
msgstr "Insérer un séparateur"

#: ../mate_menu/plugins/applications.py:767
#: ../mate_menu/plugins/applications.py:834
msgid "Launch when I log in"
msgstr "Lancer lorsque je me connecte"

#: ../mate_menu/plugins/applications.py:769
#: ../mate_menu/plugins/applications.py:836
msgid "Launch"
msgstr "Lancer"

#: ../mate_menu/plugins/applications.py:770
msgid "Remove from favorites"
msgstr "Supprimer des favoris"

#: ../mate_menu/plugins/applications.py:772
#: ../mate_menu/plugins/applications.py:839
msgid "Edit properties"
msgstr "Modifier les propriétés"

#. i18n
#: ../mate_menu/plugins/applications.py:811
msgid "Remove"
msgstr "Supprimer"

#: ../mate_menu/plugins/applications.py:833
msgid "Show in my favorites"
msgstr "Afficher dans mes favoris"

#: ../mate_menu/plugins/applications.py:837
msgid "Delete from menu"
msgstr "Supprimer du menu"

#: ../mate_menu/plugins/applications.py:888
msgid "Search Google"
msgstr "Recherche Google"

#: ../mate_menu/plugins/applications.py:895
msgid "Search Wikipedia"
msgstr "Recherche Wikipédia"

#: ../mate_menu/plugins/applications.py:905
msgid "Lookup Dictionary"
msgstr "Consultation Dictionnaire"

#: ../mate_menu/plugins/applications.py:912
msgid "Search Computer"
msgstr "Recherche sur l'ordinateur"

#: ../mate_menu/plugins/applications.py:1332
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""
"Impossible de sauvegarder les favoris. Vérifiez que vous avez les droits en "
"écriture pour ~/.config/mate-menu"

#: ../mate_menu/plugins/applications.py:1532
msgid "All"
msgstr "Tout"

#: ../mate_menu/plugins/applications.py:1532
msgid "Show all applications"
msgstr "Afficher toutes les applications"

#: ../mate_menu/plugins/system_management.py:159
msgid "Install, remove and upgrade software packages"
msgstr "Installer, supprimer et mettre à jour les paquets logiciels"

#: ../mate_menu/plugins/system_management.py:166
msgid "Configure your system"
msgstr "Configurer votre système"

#: ../mate_menu/plugins/system_management.py:176
msgid "Use the command line"
msgstr "Utiliser la ligne de commande"

#: ../mate_menu/plugins/system_management.py:187
msgid "Requires password to unlock"
msgstr "Requiert un mot de passe pour déverrouillage"

#: ../mate_menu/plugins/system_management.py:190
msgid "Logout"
msgstr "Déconnexion"

#: ../mate_menu/plugins/system_management.py:194
msgid "Log out or switch user"
msgstr "Se déconnecter ou changer d'utilisateur"

#: ../mate_menu/plugins/system_management.py:201
msgid "Shutdown, restart, suspend or hibernate"
msgstr "Éteindre, redémarrer, suspendre ou mettre en veille"

#: ../mate_menu/plugins/places.py:161
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr ""
"Parcourir tous les disques locaux et distants et tous les répertoires "
"accessibles par cet ordinateur"

#: ../mate_menu/plugins/places.py:172
msgid "Open your personal folder"
msgstr "Ouvrir votre dossier personnel"

#: ../mate_menu/plugins/places.py:185
msgid "Browse bookmarked and local network locations"
msgstr "Parcourir les emplacements référencés dans les signets et locaux"

#: ../mate_menu/plugins/places.py:206
msgid "Browse items placed on the desktop"
msgstr "Parcourir les éléments placés sur le Bureau"

#: ../mate_menu/plugins/places.py:219
msgid "Browse deleted files"
msgstr "Parcourir les fichiers supprimés"

#: ../mate_menu/plugins/places.py:272
msgid "Empty trash"
msgstr "Vider la corbeille"

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr "Documents récents"

#: ../mate_menu/keybinding.py:199
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr ""
"Cliquer pour assigner un nouveau raccourci clavier pour ouvrir et fermer le "
"menu."

#: ../mate_menu/keybinding.py:200
msgid "Press Escape or click again to cancel the operation.  "
msgstr ""
"Appuyer sur la touche Échap ou cliquer à nouveau pour annuler l'opération."

#: ../mate_menu/keybinding.py:201
msgid "Press Backspace to clear the existing keybinding."
msgstr ""
"Appuyer sur la touche Retour arrière pour supprimer la combinaison existante."

#: ../mate_menu/keybinding.py:214
msgid "Pick an accelerator"
msgstr "Sélectionner un accélérateur"

#: ../mate_menu/keybinding.py:267
msgid "<not set>"
msgstr "<not set>"
