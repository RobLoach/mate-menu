# Khmer translation for MATE Menu
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: mate-menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-03-11 22:31+0000\n"
"PO-Revision-Date: 2012-06-17 16:34+0000\n"
"Last-Translator: KOSAL (David Salvo) <slinkeepy@gmail.com>\n"
"Language-Team: Khmer <km@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-02-25 10:21+0000\n"
"X-Generator: Launchpad (build 17355)\n"

#: ../lib/mate-menu.py:70
msgid "Menu"
msgstr "ម៉ឺនុយ"

#. Fake class for MyPlugin
#: ../lib/mate-menu.py:254
msgid "Couldn't load plugin:"
msgstr ""

#: ../lib/mate-menu.py:326
msgid "Couldn't initialize plugin"
msgstr ""

#: ../lib/mate-menu.py:715
msgid "Advanced MATE Menu"
msgstr ""

#: ../lib/mate-menu.py:798
msgid "Preferences"
msgstr "ចំណូលចិត្ត"

#: ../lib/mate-menu.py:801
msgid "Edit menu"
msgstr "កែ​សម្រួល​ម៉ឺនុយ"

#: ../lib/mate-menu.py:804
msgid "Reload plugins"
msgstr ""

#: ../lib/mate-menu.py:807
msgid "About"
msgstr "អំពី"

#. i18n
#: ../lib/mate-menu-config.py:50
msgid "Menu preferences"
msgstr ""

#: ../lib/mate-menu-config.py:53
msgid "Always start with favorites pane"
msgstr ""

#: ../lib/mate-menu-config.py:54
msgid "Show button icon"
msgstr "បង្ហាញ​រូបតំណាង​ប៊ូតុង"

#: ../lib/mate-menu-config.py:55
msgid "Use custom colors"
msgstr "ប្រើ​ពណ៌​ផ្ទាល់ខ្លួន"

#: ../lib/mate-menu-config.py:56
msgid "Show recent documents plugin"
msgstr ""

#: ../lib/mate-menu-config.py:57
msgid "Show applications plugin"
msgstr ""

#: ../lib/mate-menu-config.py:58
msgid "Show system plugin"
msgstr ""

#: ../lib/mate-menu-config.py:59
msgid "Show places plugin"
msgstr ""

#: ../lib/mate-menu-config.py:61
msgid "Show application comments"
msgstr ""

#: ../lib/mate-menu-config.py:62
msgid "Show category icons"
msgstr "បង្ហាញ​រូបតំណាង​ប្រភេទ"

#: ../lib/mate-menu-config.py:63
msgid "Hover"
msgstr "សំកាំង"

#: ../lib/mate-menu-config.py:64
msgid "Remember the last category or search"
msgstr ""

#: ../lib/mate-menu-config.py:65
msgid "Swap name and generic name"
msgstr ""

#: ../lib/mate-menu-config.py:67
msgid "Border width:"
msgstr "ទទឹង​ស៊ុម​៖"

#: ../lib/mate-menu-config.py:68
msgid "pixels"
msgstr "ភីកសែល"

#: ../lib/mate-menu-config.py:70
msgid "Opacity:"
msgstr "ភាព​ស្រអាប់​៖"

#: ../lib/mate-menu-config.py:73
msgid "Button text:"
msgstr "អត្ថបទ​ប៊ូតុង៖"

#: ../lib/mate-menu-config.py:74
msgid "Options"
msgstr "ជម្រើស"

#: ../lib/mate-menu-config.py:75 ../mate_menu/plugins/applications.py:252
msgid "Applications"
msgstr "កម្មវិធីអនុវត្ត"

#: ../lib/mate-menu-config.py:77
msgid "Theme"
msgstr "ស្បែក"

#: ../lib/mate-menu-config.py:78 ../mate_menu/plugins/applications.py:249
#: ../mate_menu/plugins/applications.py:250
msgid "Favorites"
msgstr "សំណព្វ"

#: ../lib/mate-menu-config.py:79
msgid "Main button"
msgstr ""

#: ../lib/mate-menu-config.py:80
msgid "Plugins"
msgstr ""

#: ../lib/mate-menu-config.py:82
msgid "Background:"
msgstr "ផ្ទៃ​ខាងក្រោយ ៖"

#: ../lib/mate-menu-config.py:83
msgid "Headings:"
msgstr ""

#: ../lib/mate-menu-config.py:84
msgid "Borders:"
msgstr ""

#: ../lib/mate-menu-config.py:85
msgid "Theme:"
msgstr "ស្បែក៖"

#. self.builder.get_object("applicationsLabel").set_text(_("Applications"))
#. self.builder.get_object("favoritesLabel").set_text(_("Favorites"))
#: ../lib/mate-menu-config.py:89
msgid "Number of columns:"
msgstr "ចំនួន​ជួរឈរ៖"

#: ../lib/mate-menu-config.py:90 ../lib/mate-menu-config.py:91
#: ../lib/mate-menu-config.py:92 ../lib/mate-menu-config.py:93
msgid "Icon size:"
msgstr "ទំហំ​រូបតំណាង ៖"

#: ../lib/mate-menu-config.py:94
msgid "Hover delay (ms):"
msgstr ""

#: ../lib/mate-menu-config.py:95
msgid "Button icon:"
msgstr "រូបតំណាង​ប៊ូតុង៖"

#: ../lib/mate-menu-config.py:96
msgid "Search command:"
msgstr "ពាក្យ​បញ្ជាស្វែងរក៖"

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:98 ../mate_menu/plugins/places.py:58
msgid "Places"
msgstr "កន្លែង"

#: ../lib/mate-menu-config.py:99 ../lib/mate-menu-config.py:111
msgid "Allow Scrollbar"
msgstr ""

#: ../lib/mate-menu-config.py:100
msgid "Show GTK Bookmarks"
msgstr ""

#: ../lib/mate-menu-config.py:101 ../lib/mate-menu-config.py:112
msgid "Height:"
msgstr "កម្ពស់ ៖"

#: ../lib/mate-menu-config.py:102
msgid "Toggle Default Places:"
msgstr ""

#: ../lib/mate-menu-config.py:103 ../mate_menu/plugins/places.py:153
msgid "Computer"
msgstr "កុំព្យូទ័រ"

#: ../lib/mate-menu-config.py:104 ../mate_menu/plugins/places.py:164
msgid "Home Folder"
msgstr "ថត​ផ្ទះ"

#: ../lib/mate-menu-config.py:105 ../mate_menu/plugins/places.py:177
msgid "Network"
msgstr "បណ្ដាញ"

#: ../lib/mate-menu-config.py:106 ../mate_menu/plugins/places.py:198
msgid "Desktop"
msgstr "ផ្ទៃតុ"

#: ../lib/mate-menu-config.py:107 ../mate_menu/plugins/places.py:209
msgid "Trash"
msgstr "ធុងសំរាម"

#: ../lib/mate-menu-config.py:108
msgid "Custom Places:"
msgstr ""

#. Set 'heading' property for plugin
#: ../lib/mate-menu-config.py:110 ../mate_menu/plugins/system_management.py:55
msgid "System"
msgstr "ប្រព័ន្ធ"

#: ../lib/mate-menu-config.py:113
msgid "Toggle Default Items:"
msgstr ""

#: ../lib/mate-menu-config.py:114
#: ../mate_menu/plugins/system_management.py:149
#: ../mate_menu/plugins/system_management.py:152
#: ../mate_menu/plugins/system_management.py:155
msgid "Package Manager"
msgstr "កម្មវិធី​គ្រប់គ្រង​កញ្ចប់"

#: ../lib/mate-menu-config.py:115
#: ../mate_menu/plugins/system_management.py:162
msgid "Control Center"
msgstr ""

#: ../lib/mate-menu-config.py:116
#: ../mate_menu/plugins/system_management.py:169
msgid "Terminal"
msgstr "ស្ថានីយ"

#: ../lib/mate-menu-config.py:117
#: ../mate_menu/plugins/system_management.py:179
msgid "Lock Screen"
msgstr "ចាក់សោ​អេក្រង់"

#: ../lib/mate-menu-config.py:118
msgid "Log Out"
msgstr "ចេញ"

#: ../lib/mate-menu-config.py:119
#: ../mate_menu/plugins/system_management.py:197
msgid "Quit"
msgstr "បោះបង់"

#: ../lib/mate-menu-config.py:121
msgid "Edit Place"
msgstr ""

#: ../lib/mate-menu-config.py:122
msgid "New Place"
msgstr ""

#: ../lib/mate-menu-config.py:123
msgid "Select a folder"
msgstr "ជ្រើស​ថត"

#: ../lib/mate-menu-config.py:152
msgid "Keyboard shortcut:"
msgstr ""

#: ../lib/mate-menu-config.py:158
msgid "Images"
msgstr ""

#: ../lib/mate-menu-config.py:267
msgid "Name"
msgstr "ឈ្មោះ"

#: ../lib/mate-menu-config.py:268
msgid "Path"
msgstr "ផ្លូវ"

#: ../lib/mate-menu-config.py:284
msgid "Desktop theme"
msgstr "ស្បែក​ផ្ទៃតុ"

#: ../lib/mate-menu-config.py:432 ../lib/mate-menu-config.py:463
msgid "Name:"
msgstr "ឈ្មោះ ៖"

#: ../lib/mate-menu-config.py:433 ../lib/mate-menu-config.py:464
msgid "Path:"
msgstr "ផ្លូវ​៖"

#. i18n
#: ../mate_menu/plugins/applications.py:247
msgid "Search:"
msgstr "ស្វែងរក ៖"

#: ../mate_menu/plugins/applications.py:251
msgid "All applications"
msgstr ""

#: ../mate_menu/plugins/applications.py:618
#, python-format
msgid "Search Google for %s"
msgstr ""

#: ../mate_menu/plugins/applications.py:625
#, python-format
msgid "Search Wikipedia for %s"
msgstr ""

#: ../mate_menu/plugins/applications.py:641
#, python-format
msgid "Lookup %s in Dictionary"
msgstr ""

#: ../mate_menu/plugins/applications.py:648
#, python-format
msgid "Search Computer for %s"
msgstr ""

#. i18n
#: ../mate_menu/plugins/applications.py:761
#: ../mate_menu/plugins/applications.py:830
msgid "Add to desktop"
msgstr "បន្ថែម​ទៅ​លើ​ផ្ឌៃ​តុ"

#: ../mate_menu/plugins/applications.py:762
#: ../mate_menu/plugins/applications.py:831
msgid "Add to panel"
msgstr "បន្ថែម​ទៅ​បន្ទះ"

#: ../mate_menu/plugins/applications.py:764
#: ../mate_menu/plugins/applications.py:812
msgid "Insert space"
msgstr ""

#: ../mate_menu/plugins/applications.py:765
#: ../mate_menu/plugins/applications.py:813
msgid "Insert separator"
msgstr ""

#: ../mate_menu/plugins/applications.py:767
#: ../mate_menu/plugins/applications.py:834
msgid "Launch when I log in"
msgstr ""

#: ../mate_menu/plugins/applications.py:769
#: ../mate_menu/plugins/applications.py:836
msgid "Launch"
msgstr "បើក​ដំណើរការ"

#: ../mate_menu/plugins/applications.py:770
msgid "Remove from favorites"
msgstr "យក​ពី​សំណព្វ​ចេញ"

#: ../mate_menu/plugins/applications.py:772
#: ../mate_menu/plugins/applications.py:839
msgid "Edit properties"
msgstr ""

#. i18n
#: ../mate_menu/plugins/applications.py:811
msgid "Remove"
msgstr "យកចេញ"

#: ../mate_menu/plugins/applications.py:833
msgid "Show in my favorites"
msgstr ""

#: ../mate_menu/plugins/applications.py:837
msgid "Delete from menu"
msgstr "លប់​ពី​ម៉ឺនុយ​ចេញ"

#: ../mate_menu/plugins/applications.py:888
msgid "Search Google"
msgstr ""

#: ../mate_menu/plugins/applications.py:895
msgid "Search Wikipedia"
msgstr ""

#: ../mate_menu/plugins/applications.py:905
msgid "Lookup Dictionary"
msgstr ""

#: ../mate_menu/plugins/applications.py:912
msgid "Search Computer"
msgstr ""

#: ../mate_menu/plugins/applications.py:1332
msgid ""
"Couldn't save favorites. Check if you have write access to ~/.config/mate-"
"menu"
msgstr ""

#: ../mate_menu/plugins/applications.py:1532
msgid "All"
msgstr "ទាំងអស់"

#: ../mate_menu/plugins/applications.py:1532
msgid "Show all applications"
msgstr ""

#: ../mate_menu/plugins/system_management.py:159
msgid "Install, remove and upgrade software packages"
msgstr "ដំឡើង យកចេញ និង​ធ្វើឲ្យ​កញ្ចប់​កម្មវិធី​ប្រសើរ​ឡើង"

#: ../mate_menu/plugins/system_management.py:166
msgid "Configure your system"
msgstr "កំណត់​រចនាសម្ព័ន្ធ​ប្រព័ន្ធ​របស់​អ្នក"

#: ../mate_menu/plugins/system_management.py:176
msgid "Use the command line"
msgstr "ប្រើ​បន្ទាត់​ពាក្យបញ្ជា"

#: ../mate_menu/plugins/system_management.py:187
msgid "Requires password to unlock"
msgstr ""

#: ../mate_menu/plugins/system_management.py:190
msgid "Logout"
msgstr "ចេញ"

#: ../mate_menu/plugins/system_management.py:194
msgid "Log out or switch user"
msgstr ""

#: ../mate_menu/plugins/system_management.py:201
msgid "Shutdown, restart, suspend or hibernate"
msgstr ""

#: ../mate_menu/plugins/places.py:161
msgid ""
"Browse all local and remote disks and folders accessible from this computer"
msgstr "រក​មើល​ថាស​មូលដ្ឋាន ​ថាស​ពី​ចម្ងាយ និង​ថត​ដែល​អាច​ចូល​ដំណើរការ​បាន​ពី​កុំព្យូទ័រ​​នេះ"

#: ../mate_menu/plugins/places.py:172
msgid "Open your personal folder"
msgstr "បើក​ថត​ផ្ទាល់ខ្លួន​របស់​អ្នក"

#: ../mate_menu/plugins/places.py:185
msgid "Browse bookmarked and local network locations"
msgstr ""

#: ../mate_menu/plugins/places.py:206
msgid "Browse items placed on the desktop"
msgstr ""

#: ../mate_menu/plugins/places.py:219
msgid "Browse deleted files"
msgstr ""

#: ../mate_menu/plugins/places.py:272
msgid "Empty trash"
msgstr "សម្អាត​ធុងសំរាម"

#. Set 'heading' property for plugin
#: ../mate_menu/plugins/recent.py:48
msgid "Recent documents"
msgstr "ឯកសារ​ថ្មីៗ"

#: ../mate_menu/keybinding.py:199
msgid "Click to set a new accelerator key for opening and closing the menu.  "
msgstr ""

#: ../mate_menu/keybinding.py:200
msgid "Press Escape or click again to cancel the operation.  "
msgstr ""

#: ../mate_menu/keybinding.py:201
msgid "Press Backspace to clear the existing keybinding."
msgstr ""

#: ../mate_menu/keybinding.py:214
msgid "Pick an accelerator"
msgstr ""

#: ../mate_menu/keybinding.py:267
msgid "<not set>"
msgstr ""

#~ msgid "Please wait, this can take some time"
#~ msgstr "សូម​រង់ចាំ វា​អាច​ចំណាយ​ពេល​មួយ​ស្របក់ ។"

#~ msgid "Remove %s?"
#~ msgstr "យក %s ចេញ?"
